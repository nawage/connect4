//Derived from stackoverflow question: https://stackoverflow.com/questions/30678792/how-to-add-physics-to-css-animations

var v = {x: 0, y: 0},       // some vector
    pos = {x: 0, y: -800},    // some position
    g = 0.5,                  // some gravity
    absorption = 0.2,         // friction/absorption
    bottom = 0,             // floor collision
    maxFrames = 220,          // so we can normalize
    step = 1,                // grab every nth + bounce
    heights = [],             // collect in an array as step 1
    css = "";                 // build CSS animation

// calc CSS-frames
for(var i = 0; i <= maxFrames; i++) {
    var t = i / maxFrames;
    pos.x += v.x;               // update position with vector
    pos.y += v.y;
    v.y += g;                   // update vector with gravity

    if (pos.y > bottom) {
        pos.y = bottom;
        v.y = -v.y * absorption;
        heights.push({pst: t * 100, y: pos.y});
    }
    else if (!(i % step)) {heights.push({pst: t * 100, y: pos.y})}
}

// step 2: format height-array into CSS
css += "@keyframes drop {\n";
for(i = 0; i < heights.length; i++) {
    var e = heights[i];
    css += "  " + e.pst.toFixed(3) + "% {transform: translateY(" + e.y.toFixed(3) + "px)}\n";
}
css += "}";

console.log(css);

// document.write("<pre>" + css + "</pre>");

