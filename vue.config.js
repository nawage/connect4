module.exports = {
    "transpileDependencies": [
        "vuetify"
    ],
    pwa: {
        name: "Connect4",
        themeColor: "#000000",
        msTileColor: "#FFFF00",
        appleMobileWebAppCache: "yes",
        manifestOptions: {
            background_color: "#FFFF00",
            scope: "/",
            start_url: "/",
            publicPath: "./",
            name: "Connect4",
            app_name: "Connect4",
            short_name: "Connect4",
        },
        iconPaths: {
            favicon32: 'img/icons/favicon-32-32.png',
            favicon16: 'img/icons/favicon-16-16.png',
            appleTouchIcon: 'img/icons/Square150x150Logo.scale-150.png',
            maskIcon: 'img/icons/safari-pinned-tab.svg',
            msTileImage: 'img/icons/windows-squarelogo-150-150.png'
        }
    }
}