# Connect 4 Project

Live Demo: 

https://shopknightsproject-jyite.mongodbstitch.com

Notes:

1. This application is a [PWA](https://en.wikipedia.org/wiki/Progressive_web_application), and can be installed locally and run offline
2. This application requires an internet connection for the automated yellow player
3. This application is designed to run on mobile devices

# Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Launches the Vue project management UI
```
vue ui
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Project Objectives

1. Make the game of Connect Four.
2. It needs to be multiplayer, and can be so locally.

# Development Milestones

### 11/19/2020 (2h):
Focus: *Initial setup and configuration of project*

1. Completed initial configuration of Vue2 project
2. Added Vuetify plugin for Material Design UI elements
3. Initialized Realm instance to host SPA
4. Tested initial website with Realm hosting to verify pipeline

### 11/20/2020 / 11/21/2020 (4h:30m):
Focus: *Research and development of core features*


[Found github repo for connect 4 solver](https://github.com/PascalPons/connect4)


1. Game Piece Images Created

2. Game Piece Dropping Animation

    - Researched Javascript Canvas solution, determined too be too hard to integrate with Vue.
    
    - Researched CSS animation solution, found physics simulation CSS animation generator:
      https://stackoverflow.com/questions/30678792/how-to-add-physics-to-css-animations
    
    - Implemented initial CSS Animation test, showed promise
    
    - Started conversion to vue component to be used dynamically in game board grid, stuck on animation playing

3. AI / Game solving Algorithms
    - Found promising Gome Solving Altorithm developed by Pascal Pons
        -  (read in brief to understand general idea)
    
        - [Github repo with connect 4 solver source code](https://github.com/PascalPons/connect4)
    
        - Considered possibility of reimplementing algorithm, decided against this based on 48h time constraint
    
        - Reverse engineered [connect4.gamesolver.org](https://connect4.gamesolver.org/) and discovered underlying "solve" api endpoint for generating AI moves
    
        - Created simple demo which sent http requests and obtained AI moves based on Pascal Pons' algorithm
    
        - Determined http AI solution was viable to implement
    
    - Implemented http request AI core into root vue component

### 11/21/2020 (13h):
Focus: *Continued development of core features*

1. Game board state logic

    - Added data elements for game board, player turn, turn lock (for AI), board reset
    
2. Game Win Detection

    - Found [stackoverflow question](https://stackoverflow.com/a/38211417) related to goal: 
    
    - Implemented after modifying to fit use case
    
3. UI elements added

    - finalized board resizing
    
    - finalized game piece drop physics
    
    - finalized control button layout / styling
    
4. Yellow AI added and finalized

    - Implemented http request function for AI
    
5. Code documentation

    - Connect4Piece.vue
    
    - GamePage.vue
    
## Total Project Time (19h:30m)